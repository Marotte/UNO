import paramiko, socket, binascii, random, string

from multiprocessing import Process, Queue, Pool

from time import sleep, time

import Logger, Run

class SSHClient:

    def __init__(self, privkeyfile = ''):
    
        self.key    = paramiko.RSAKey.from_private_key(open(privkeyfile, 'r'))
        self.logger = Logger.Logger()
        
        self.logger.log(0, 'Using key: '+binascii.hexlify(self.key.get_fingerprint()).decode('utf8'))

    def execute(self, command, host, user, q):

        try:

            exec_status = ''
            return_code = -1
            self.client = paramiko.SSHClient()
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.load_system_host_keys()
            self.client.connect(host, username=user, password='', pkey=self.key)
            start  = time()
            std    = self.client.exec_command(command)
            end    = time()
            return_code = std[1].channel.recv_exit_status() 
            stdout = list(std[1])
            stderr = list(std[2])
            q.put((host, user, return_code, stdout, stderr, exec_status, command, start, end))
            self.client.close()

        except (paramiko.ssh_exception.AuthenticationException,
                paramiko.ssh_exception.NoValidConnectionsError) as e:
            
            self.logger.log(1, str(e))
            exec_status = str(e)
            q.put((host, user, -1, [], [], exec_status, command, 0, 0))
            self.client.close()

    def execute_cmds(self, commands, host, user):

        q = Queue()
        runs = []

        for cmd in commands:
        
            if not cmd:
                
                continue
        
            Process(target=self.execute, args=(cmd, host, user, q)).start()

        for cmd in commands:
            
            runs.append(Run.Run(*q.get()))
            
        return(runs)    
        

    def execute_hosts(self, hosts, command, user):

        q = Queue()
        runs = []

        for host in hosts:
        
            if not host:
                
                continue
        
            Process(target=self.execute, args=(command, host, user, q)).start()

        for host in hosts:
            
            runs.append(Run.Run(*q.get()))
            
        return(runs)    

    def execute_job(self, job):
        
        try:

            exec_status = ''
            return_code = -1
            self.client = paramiko.SSHClient()
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.load_system_host_keys()
            self.client.connect(job[2], username=job[3], password='', pkey=self.key)
            start  = time()
            std    = self.client.exec_command(job[1])
            end    = time()
            return_code = std[1].channel.recv_exit_status() 
            stdout = list(std[1])
            stderr = list(std[2])
            run = (job[2], job[3], return_code, stdout, stderr, exec_status, job[1], start, end)
            self.client.close()

        except (paramiko.ssh_exception.AuthenticationException,
                paramiko.ssh_exception.NoValidConnectionsError) as e:
            
                self.logger.log(1, str(e))
                exec_status = str(e)
                run = (job[2], job[3], -1, [], [], exec_status, job[1], 0, 0)
                self.client.close()
            
        return(Run.Run(*run))    

    def execute_chain(self, chain, brk = False):
        
        runs = []
        last_rc = 0
        last_out = ''
                
        instance = ''.join(random.SystemRandom().choice(string.hexdigits) for _ in range(8)).upper()
        
        for job in chain:

            self.logger.log(0, 'CHAIN '+instance+': '+str(job))
            
            if last_rc == int(job[4]):
                
                run = Run.Run(*self.execute_job(job))
                runs.append(run)
                last_rc = run[2]
                last_out = run[3]

                continue

            elif job[5].match(str(last_out)):

                run = Run.Run(*self.execute_job(job))
                runs.append(run)
                last_rc = run[2]
                last_out = run[3]

                continue

            else:
                
                runs.append(Run.Run(*(job[2], job[3], -2, [], [], 'NOGO', job[1], 0, 0)))
                
                if brk:

                    break

        return(runs)


    def execute_pool(self, jobs):

        q = Queue()
        runs = []
        
        instance = ''.join(random.SystemRandom().choice(string.hexdigits) for _ in range(8)).upper()

        for job in jobs:
        
            self.logger.log(0, 'POOL '+instance+': '+str(job))
            Process(target=self.execute, args=(job[1], job[2], job[3], q)).start()

        for job in jobs:
            
            runs.append(Run.Run(*q.get()))
            
        return(runs)    

    def execute_joblist(self, joblist, mode = 'chain', brk = False):
        
        if mode == 'chain':

            return(self.execute_chain(joblist.jobs, brk))
            
        elif mode == 'pool':

            return(self.execute_pool(joblist.jobs))    
