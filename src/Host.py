import socket, ipaddress

import Logger

class Host:

    logger = Logger.Logger()

    def __init__(self, db):
        
        self.db = db
    
    def hosts(self, host_filter = '%'):
        
        return(self.db.execute('SELECT * FROM host WHERE hostname LIKE ? OR address LIKE ?', (host_filter, host_filter)))

    def addresses(self, host_filter = '%'):
        
        ret = []
        addresses = self.db.execute('SELECT address FROM host WHERE hostname LIKE ? OR address LIKE ?', (host_filter, host_filter))
        
        for addr in addresses:
            
            ret.append(addr[0])
            
        return(ret)    

    def add(self, hostname = '', address = ''):
        
        if not address:
            
            address = '127.0.0.1'
            
        try:
            
            ipaddress.ip_address(address)
            
        except ValueError as e:
            
            self.logger.log(1, str(e))
            
            return(False)        
        
        if not hostname:
            
            try:
                
                hostname = socket.gethostbyaddr(address)[0]
                
            except socket.herror as e:
                
                hostname = address    
        
        self.db.execute('INSERT OR IGNORE INTO host (hostname, address) VALUES (?, ?)', (hostname, address))
        self.db.commit()

    def add_net(self, network):
        
        net = ipaddress.IPv4Network(network)
        hosts = []
        
        for ip in net.hosts():
            
            try:
                
                hostname = socket.gethostbyaddr(str(ip))[0]
                
            except socket.herror as e:
                
                hostname = str(ip)

            hosts.append((hostname, str(ip)))
            
        self.db.cursor.executemany('INSERT OR IGNORE INTO host (hostname, address) VALUES (?, ?)', hosts)
        self.db.commit() 

    def del_host(self, key = ''):

        self.db.execute('DELETE FROM host WHERE hostname = ? OR address = ?', (key, key))
        self.db.commit()
