#!/usr/bin/env python3

import sys, os, pwd, socket, ipaddress

from pprint import pprint

sys.path.append('src')

import SQLiteDatabase
import SSHClient
import Host, Cmd, Run, Job, JobList

BASENAME  = os.path.basename(sys.argv[0]).split('.')[:-1][0]
DBFILE    = BASENAME+'.sqlite'
PRIVKFILE = BASENAME+'_id'
PUBKFILE  = BASENAME+'_id.pub'
USER      = pwd.getpwuid(os.getuid()).pw_name
HOST      = socket.gethostbyname(socket.gethostname())

# Demo

## Connect (or create) a database
db = SQLiteDatabase.SQLiteDatabase(DBFILE)

## Initial database setting (only needed once)
db.setup('src/uno.sql')

## Create a 'host' controller
host = Host.Host(db)

## Add some hosts
host.add(address='192.168.0.20')
host.add(address='192.168.0.45')

## Create a 'cmd' controller
cmd = Cmd.Cmd(db)

## Add some commands 
cmd.add('UPTIME','uptime')
cmd.add('DF','df -Pk')
cmd.add('PWD','pwd')
cmd.add('FREE','free -k')
cmd.add('SSH','ssh -V')
cmd.add('CPU_MODEL','cat /proc/cpuinfo |grep "^model name" |uniq |cut -d":" -f2')

#~ # Show hosts
#~ print(host.hosts())
#~ print(host.hosts('i%'))
#~ print(host.hosts('%.0.%'))

#~ # Show commands
#~ print(cmd.cmds())
#~ print(cmd.cmds('u%'))

#~ # Get command line by name and vice versa
#~ print(cmd.cmdline('UPTIME'))
#~ print(cmd.name('df -Pk'))

# Create a SSH client
ssh = SSHClient.SSHClient(PRIVKFILE)

# Execute some commands in parallel on a given host
#~ pprint(ssh.execute_cmds(['date', 'uname -a', 'whoami','uptime','exit 42'], HOST, USER))
#~ pprint(ssh.execute_cmds(['echo -e "plop\nplop\nplop\n"', 'whoami','uptime'], '192.168.0.45', 'root'))

# Execute a given command in parallel on some hosts
#~ pprint(ssh.execute_hosts(['192.168.0.18', '192.168.0.45'], 'ssh -V', 'root'))
#~ pprint(ssh.execute_hosts(['192.168.0.45', '192.168.0.18','192.168.0.10'], 'cat /etc/issue', 'root'))

# Run controller
run = Run.Run()

# Store a list of 'runs' in the database
#~ pprint(run.store(ssh.execute_hosts(['192.168.0.45', '192.168.0.18','192.168.0.10'], 'cat /etc/issue', 'root'), db))
#~ pprint(run.store(ssh.execute_cmds(['date', 'uname -a', 'whoami','uptime','pwd'], HOST, USER), db))

# Create jobs
ja = Job.Job(db, '192.168.0.20', USER, 'UPTIME', '0')
jb = Job.Job(db, '192.168.0.20', USER, 'DF','0')
jc = Job.Job(db, '192.168.0.45', cmdname='UPTIME', prev='/.*sda4.*/')
jd = Job.Job(db, '192.168.0.45', cmdname='PWD', prev='/.*666,.*/')
je = Job.Job(db, '192.168.0.20', USER, 'FREE','0')

# Store a single job
ja.store()

#~ print(str(ja))

#~ # Run one single job (return a 'run' instance)
pprint(ssh.execute_job(ja))

# Create a job list and execute it. Default mode is 'chained, break = False'
joblist = JobList.JobList([ja, jb, jc, jd, je], db)
print('\n'.join(map(str, joblist.jobs))+'\n==========')

runs = ssh.execute_joblist(joblist)

# Store the joblist
joblist.store()

# Execute again with break = True
runs += ssh.execute_joblist(joblist, brk = True)

# Execute again in mode 'pool' : all jobs are ran in parallel 
runs += ssh.execute_joblist(joblist, mode='pool')

# Store results
run.store(runs, db)

# Store joblist
job_ctrl = Job.Job(db) # Not necessary, we could use any instance of Job to store a list of jobs
job_ctrl.store(joblist, db)

# Add some more hosts
host.add_net('192.168.0.0/24')

# Execute the 'SSH' command on all hosts (as root)
runs = ssh.execute_hosts(host.addresses(), cmd.cmdline('SSH'), 'root')
run.store(runs, db)

# Make another job list
jx = Job.Job(db, '192.168.0.20', USER, 'UPTIME', '0')
jy = Job.Job(db, '192.168.0.20', USER, 'CPU_MODEL','0')
joblist2 = JobList.JobList([jx, jy], db)

# Execute and store results for this new job list (mode = pool)
run.store(ssh.execute_joblist(joblist2, mode='pool'), db)

# Store both joblists
jbl_ctrl = JobList.JobList() # Not necessary, we could use any instance of JobList to store a list of joblists.
jbl_ctrl.store([joblist, joblist2], db)






