
class Run:

    def __init__(self, host = '', user = '', return_code = -1, stdout = '', stderr = '', exec_status = '', command = '', start = 0, end = 0):
    
        self.host        = host
        self.user        = user
        self.return_code = return_code
        self.stdout      = stdout
        self.stderr      = stderr
        self.exec_status = exec_status
        self.command     = command
        self.start       = start
        self.end         = end

    def __getitem__(self, i):
        
        return((self.host, self.user, self.return_code, self.stdout, self.stderr, self.exec_status, self.command, self.start, self.end)[i])
 
    def __repr__(self):

        command = self.command.encode('unicode-escape').decode()
        
        if self.return_code == 0:
            
            output = self.stdout
            
            if output == []:
                
                output = self.stderr
            
            return('OK:\t'+self.user+'@'+self.host+' \''+command+'\' '+str(output)+' '+str(self.start)+' '+str(self.end))
        
        elif self.return_code == -1:
            
            return('NORUN:\t'+self.user+'@'+self.host+' \''+command+'\' '+str(self.exec_status))

        elif self.return_code == -2:
            
            return('NOGO:\t'+self.user+'@'+self.host+' \''+command+'\' '+str(self.exec_status))

        else:
            
            return('ERROR:\t'+self.user+'@'+self.host+' \''+command+'\' '+str(self.stdout)+' '+str(self.stderr)+' '+str(self.start)+' '+str(self.end))

    def store(self, runs = [], db = None):
        
        if not db or type(runs) is not list:
            
            return(False)
        
        if len(runs) == 0:
        
            db.execute('INSERT OR IGNORE INTO run (host, user, return_code, stdout, stderr, exec_status, command, start, end) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
                            (self.host,
                             self.user,
                             self.return_code,
                             ''.join(self.stdout),
                             ''.join(self.stderr),
                             self.exec_status,
                             self.command,
                             self.start,
                             self.end
                             )
                            )
            return([runs]) 

        else:
            
            _runs = []
            
            for r in runs:
                
                stdout = ''.join(r[3])
                stderr = ''.join(r[4])
                
                _runs.append((r[0],r[1],r[2],stdout,stderr,r[5],r[6],r[7],r[8]))
                
            db.cursor.executemany('INSERT OR IGNORE INTO run (host, user, return_code, stdout, stderr, exec_status, command, start, end) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', _runs)

        db.commit()
        return(_runs)                              
