#!/usr/bin/env python3

import sys, os, pwd, socket
import argparse

from pprint import pprint

sys.path.append('src')

import SQLiteDatabase
import SSHClient
import Logger
import Host, Cmd, Run, Job, JobList

BASENAME  = os.path.basename(sys.argv[0]).split('.')[:-1][0]
DBFILE    = BASENAME+'.sqlite'
PRIVKFILE = BASENAME+'_id'
PUBKFILE  = BASENAME+'_id.pub'
USER      = pwd.getpwuid(os.getuid()).pw_name
HOST      = socket.gethostbyname(socket.gethostname())

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(help='Action', dest='action')

object_types = ['host','cmd','job','joblist','run']
runnable_object_types = ['job','joblist']

# List action
list_parser = subparsers.add_parser('list', help='List objects')
list_parser.add_argument('type', choices=object_types, action='store', help='Object type')
list_parser.add_argument('filter', nargs='?', type=str, action='store', default='%', help='Filter')

# Add action
create_parser = subparsers.add_parser('add', help='Add an object')
create_parser.add_argument('type', choices=object_types, action='store', help='Object type')
create_parser.add_argument('args', type=str, nargs='*', default=[], action='store',
                           help='Arguments',
                           )
# Del action
delete_parser = subparsers.add_parser('del', help='Remove an object')
delete_parser.add_argument('type', choices=object_types, action='store', help='Object type')
delete_parser.add_argument('key', nargs=1, default=False, action='store',
                           help='Key',
                           )

# Run action
create_parser = subparsers.add_parser('run', help='Run object')
create_parser.add_argument('type', choices=runnable_object_types, action='store', help='Object type')
create_parser.add_argument('args', type=str, nargs='*', default=[], action='store',
                           help='Arguments',
                           )

args = parser.parse_args()

db = SQLiteDatabase.SQLiteDatabase(DBFILE)
db.setup('src/uno.sql')
ssh = SSHClient.SSHClient(PRIVKFILE)
logger = Logger.Logger()

if args.action == 'list':

    if args.type == 'host':
        
        host = Host.Host(db)

        for h in host.hosts(args.filter):
            
            print('\t'.join(h))

    if args.type == 'job':
        
        job = Job.Job(db)

        for j in job.jobs(args.filter):
            
            print(':'.join(j))

elif args.action == 'add':

    if args.type == 'host':
        
        host = Host.Host(db)
        host.add(address=args.args[0])

elif args.action == 'del':

    if args.type == 'host':
        
        host = Host.Host(db)
        host.del_host(args.key[0])

elif args.action == 'run':

    if args.type == 'job':

        if len(args.args) < 3:
            
            logger.log(1,'3 arguments needed: cmdname, host, user.')

        else:
        
            job = Job.Job(db, cmdname=args.args[0], host=args.args[1], user=args.args[2])
            run = ssh.execute_job(job)
            print('Job ran: '+run[1]+'@'+run[0]+'\t`'+run[6]+'` ('+str(run[2])+')', file=sys.stderr)
            print(''.join(run.stdout))
