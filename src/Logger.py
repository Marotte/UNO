import sys

class Logger:

    log_level = 0 # Log everything

    def __init__(self):
    
        self.output = sys.stderr
        
    def log(self, level, message):
    
        if level >= self.log_level:
        
            print(message, file=self.output)
