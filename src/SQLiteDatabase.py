import os, sqlite3

class SQLiteDatabase:

    def __init__(self, dbfile = ':memory:'):
    
        self.connection = sqlite3.connect(dbfile)
        self.cursor     = self.connection.cursor()

    def execute_script(self, script):
        
        self.cursor.executescript(script)    
        
    def commit(self):
        
        self.connection.commit()    
        
    def setup(self, sqlfile):

        script = open(sqlfile, 'r').read()
        self.execute_script(script)
        self.commit()

    def execute(self, query, values):
        
        return(self.cursor.execute(query, values).fetchall())
