
from collections.abc import Sequence
import random, string

class JobList(Sequence):

    def __init__(self, jobs = [], db = None):
    
        self.jobs = jobs
        self.db = db
        super().__init__()
        
    def __getitem__(self, i):
        
        return(self.jobs[i])
        
    def __len__(self):
        
        return(len(self.jobs))
        
    def store(self, joblists = [], db = None):
        
        if not db:
            
            db = self.db
        
        if len(joblists) == 0:
        
            name = ''.join(random.SystemRandom().choice(string.hexdigits) for _ in range(8)).upper()
            jobs = '\n'.join(map(str, self.jobs))
        
            db.execute('INSERT OR IGNORE INTO joblist (name, jobs) VALUES (?, ?)',
                            (name,
                             jobs,
                             )
                            )
            return([joblists]) 

        else:
            
            _joblists = []
            
            for jl in joblists:

                name = ''.join(random.SystemRandom().choice(string.hexdigits) for _ in range(8)).upper()
                jobs = '\n'.join(map(str, jl.jobs))
                _joblists.append((name, jobs))
                
            db.cursor.executemany('INSERT OR IGNORE INTO joblist (name, jobs) VALUES (?, ?)', _joblists)

        db.commit()
        return(_joblists)     
        
