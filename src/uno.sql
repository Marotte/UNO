-- UNO SQL

CREATE TABLE IF NOT EXISTS host (hostname TEXT,
                                 address  TEXT,
                                 PRIMARY KEY(hostname, address)
                                );
                                
CREATE TABLE IF NOT EXISTS cmd  (name     TEXT,
                                 cmdline  TEXT,
                                 PRIMARY KEY(name, cmdline)
                                );
                                
CREATE TABLE IF NOT EXISTS run  (id          INTEGER PRIMARY KEY AUTOINCREMENT,
                                 host        TEXT,
                                 user        TEXT,
                                 return_code INT,
                                 stdout      TEXT,
                                 stderr      TEXT,
                                 exec_status TEXT,
                                 command     TEXT,
                                 start       FLOAT,
                                 end         FLOAT
                                );

CREATE TABLE IF NOT EXISTS job  (cmd        TEXT,
                                 host       TEXT,
                                 user       TEXT,
                                 prev       TEXT,
                                 PRIMARY KEY(cmd, host, user, prev)
                                );
                                
CREATE TABLE IF NOT EXISTS joblist  (name        TEXT PRIMARY KEY,
                                     jobs        TEXT UNIQUE
                                );



