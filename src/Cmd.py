

class Cmd:

    def __init__(self, db):
        
        self.db = db
    
    def cmds(self, cmd_filter = '%'):
        
        return(self.db.execute('SELECT * FROM cmd WHERE name LIKE ?', (cmd_filter,)))

    def add(self, name = '', cmdline = ''):

        self.db.execute('INSERT OR IGNORE INTO cmd (name, cmdline) VALUES (?, ?)', (name, cmdline))
        self.db.commit()

    def cmdline(self, name):
        
        res = self.db.execute('SELECT cmdline FROM cmd WHERE name = ?', (name,))
        
        if len(res) > 0:
        
            return(res[0][0])
            
        else:
            
            return('')    

    def name(self, cmdline):
        
        res = self.db.execute('SELECT name FROM cmd WHERE cmdline = ?', (cmdline,))
        
        if len(res) > 0:
        
            return(res[0][0])
            
        else:
            
            return('') 


