import re

import Cmd

class Job:

    def __init__(self, db, host = '', user = 'root', cmdname = '', prev = '0'):
        
        self.db = db
        self.cmd_ctrl    = Cmd.Cmd(self.db)
        self.cmdname     = cmdname
        self.cmdline     = self.cmd_ctrl.cmdline(cmdname)
        self.host        = host
        self.user        = user
        self.prev        = prev
        self.prev_rc     = -1
        self.prev_out_re = re.compile('no^')

        if re.match('^/.*/$', prev):
            
            self.prev_out_re = re.compile(prev.strip('/'))
            
        elif re.match('[0-9]{1,3}', prev):
            
            self.prev_rc = int(prev)
            
    def __repr__(self):
        
        return(self.prev+':'+self.cmdname+':'+self.user+':'+self.host)
            
    def __getitem__(self, i):
        
        return((self.cmdname, self.cmdline, self.host, self.user, self.prev_rc, self.prev_out_re, self.prev)[i])

    def jobs(self, cmd_filter = '%'):
        
        return(self.db.execute('SELECT prev,cmd,user,host FROM job WHERE cmd LIKE ?', (cmd_filter,)))

    def store(self, jobs = [], db = None):
        
        if not db:
            
            db = self.db
        
        if len(jobs) == 0:
        
            db.execute('INSERT OR IGNORE INTO job (cmd, host, user, prev) VALUES (?, ?, ?, ?)',
                            (self.cmdname,
                             self.host,
                             self.user,
                             self.prev
                             )
                            )
            return([jobs]) 

        else:
            
            _jobs = []
            
            for j in jobs:

                _jobs.append((j[0],j[2],j[3],j[6]))
                
            db.cursor.executemany('INSERT OR IGNORE INTO job (cmd, host, user, prev) VALUES (?, ?, ?, ?)', _jobs)

        db.commit()
        return(_jobs)    
        
